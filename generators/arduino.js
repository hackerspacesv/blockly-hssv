/**
 * @license
 * Visual Blocks Language
 *
 * Copyright 2012 Google Inc.
 * https://developers.google.com/blockly/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @fileoverview Helper functions for generating Arduino code for blocks.
 * @author mario.gomez@teubi.co (Mario Gomez)
 */
'use strict';

goog.provide('Blockly.Arduino');

goog.require('Blockly.Generator');


/**
 * JavaScript code generator.
 * @type {!Blockly.Generator}
 */
Blockly.Arduino = new Blockly.Generator('Arduino');

/**
 * List of illegal variable names.
 * This is not intended to be a security feature.  Blockly is 100% client-side,
 * so bypassing this list is trivial.  This is intended to prevent users from
 * accidentally clobbering a built-in object or function.
 * @private
 */
Blockly.Arduino.addReservedWords(
  'Blockly,' +  // In case JS is evaled in the current window.
  // https://en.cppreference.com/w/cpp/keyword
  'alignas,alignof,and,and_eq,asm,atomic_cancel,atomic_commit,atomic_noexcept,' +
  'auto,bitand,bitor,bool,break,case,catch,char,char16_t,char32_t,class,compl,' +
  'concept,const,constexpr,const_cast,continue,co_await,co_return,co_yield,' +
  'decltype,default,delete,do,double,dynamic_cast,else,enum,explicit,export,' +
  'false,float,for,friend,goto,if,import,inline,int,long,module,mutable,namespace,' +
  'new,noexcept,not,not_eq,nullptr,operator,or,or_eq,private,protected,public,' +
  'reflexpr,register,reinterpret_cast,requires,return,short,signed,sizeof,static,' +
  'static_assert,static_cast,struct,switch,synchronized,template,this,thread_local,' +
  'throw,true,try,typedef,typeid,typename,union,unsigned,using,virtual,void,volatile,' +
  'wchar_t,while,xor,xor_ec,override,final,audit,axiom,transaction_safe,' +
  'transaction_safe_dynamic,if,elif,endif,ifdef,ifndef,define,undef,' +
  'include,line,error,pragma,defined,__has_include,__has_cpp_attribute,' +
  // https://www.arduino.cc/reference/en/
  'digitalRead,digitalWrite,pinMode,analogRead,analogReference,analogWrite,' +
  'analogReadResolution,analogWriteResolution,noTone,pulseIn,pulseInLong,' +
  'shiftIn,shiftOut,tone,delay,delayMicroseconds,micros,millis,abs,constrain,' +
  'map,max,min,pow,sq,sqrt,cos,sin,tan,isAlpha,isAlphaNumeric,isAscii,isControl,' +
  'isDigit,isGraph,isHexadecimalDigit,isLowercase,isPrintable,isPunct,' +
  'isSpace,isUpperCase,isWhitespace,random,randomSeed,bit,bitClear,bitRead,' +
  'bitSet,bitWrite,highByte,lowByte,attachInterrupt,detachInterrupt,' +
  'interrupts,noInterrupts,Serial,stream,Keyboard,Mouse,' +
  'HIGH,LOW,INPUT,OUTPUT,INPUT_PULLUP,LED_BUILTIN,byte,word,String,array,' +
  'boolean,string,scope,PROGMEM,loop,setup'
);

/**
 * Order of operation ENUMs.
 * https://en.cppreference.com/w/cpp/language/operator_precedence
 */
Blockly.Arduino.ORDER_ATOMIC = 0;           // 0 "" ...
Blockly.Arduino.ORDER_NEW = 1.1;            // new
Blockly.Arduino.ORDER_MEMBER = 1.2;         // . []
Blockly.Arduino.ORDER_FUNCTION_CALL = 2;    // ()
Blockly.Arduino.ORDER_INCREMENT = 3;        // ++
Blockly.Arduino.ORDER_DECREMENT = 3;        // --
Blockly.Arduino.ORDER_BITWISE_NOT = 4.1;    // ~
Blockly.Arduino.ORDER_UNARY_PLUS = 4.2;     // +
Blockly.Arduino.ORDER_UNARY_NEGATION = 4.3; // -
Blockly.Arduino.ORDER_LOGICAL_NOT = 4.4;    // !
Blockly.Arduino.ORDER_TYPEOF = 4.5;         // typeof
Blockly.Arduino.ORDER_VOID = 4.6;           // void
Blockly.Arduino.ORDER_DELETE = 4.7;         // delete
Blockly.Arduino.ORDER_AWAIT = 4.8;          // await
Blockly.Arduino.ORDER_EXPONENTIATION = 5.0; // **
Blockly.Arduino.ORDER_MULTIPLICATION = 5.1; // *
Blockly.Arduino.ORDER_DIVISION = 5.2;       // /
Blockly.Arduino.ORDER_MODULUS = 5.3;        // %
Blockly.Arduino.ORDER_SUBTRACTION = 6.1;    // -
Blockly.Arduino.ORDER_ADDITION = 6.2;       // +
Blockly.Arduino.ORDER_BITWISE_SHIFT = 7;    // << >> >>>
Blockly.Arduino.ORDER_RELATIONAL = 8;       // < <= > >=
Blockly.Arduino.ORDER_IN = 8;               // in
Blockly.Arduino.ORDER_INSTANCEOF = 8;       // instanceof
Blockly.Arduino.ORDER_EQUALITY = 9;         // == != === !==
Blockly.Arduino.ORDER_BITWISE_AND = 10;     // &
Blockly.Arduino.ORDER_BITWISE_XOR = 11;     // ^
Blockly.Arduino.ORDER_BITWISE_OR = 12;      // |
Blockly.Arduino.ORDER_LOGICAL_AND = 13;     // &&
Blockly.Arduino.ORDER_LOGICAL_OR = 14;      // ||
Blockly.Arduino.ORDER_CONDITIONAL = 15;     // ?:
Blockly.Arduino.ORDER_ASSIGNMENT = 16;      // = += -= **= *= /= %= <<= >>= ...
Blockly.Arduino.ORDER_YIELD = 17;         // yield
Blockly.Arduino.ORDER_COMMA = 18;           // ,
Blockly.Arduino.ORDER_NONE = 99;            // (...)

/**
 * List of outer-inner pairings that do NOT require parentheses.
 * @type {!Array.<!Array.<number>>}
 */
Blockly.Arduino.ORDER_OVERRIDES = [
  // (foo()).bar -> foo().bar
  // (foo())[0] -> foo()[0]
  [Blockly.Arduino.ORDER_FUNCTION_CALL, Blockly.Arduino.ORDER_MEMBER],
  // (foo())() -> foo()()
  [Blockly.Arduino.ORDER_FUNCTION_CALL, Blockly.Arduino.ORDER_FUNCTION_CALL],
  // (foo.bar).baz -> foo.bar.baz
  // (foo.bar)[0] -> foo.bar[0]
  // (foo[0]).bar -> foo[0].bar
  // (foo[0])[1] -> foo[0][1]
  [Blockly.Arduino.ORDER_MEMBER, Blockly.Arduino.ORDER_MEMBER],
  // (foo.bar)() -> foo.bar()
  // (foo[0])() -> foo[0]()
  [Blockly.Arduino.ORDER_MEMBER, Blockly.Arduino.ORDER_FUNCTION_CALL],

  // !(!foo) -> !!foo
  [Blockly.Arduino.ORDER_LOGICAL_NOT, Blockly.Arduino.ORDER_LOGICAL_NOT],
  // a * (b * c) -> a * b * c
  [Blockly.Arduino.ORDER_MULTIPLICATION, Blockly.Arduino.ORDER_MULTIPLICATION],
  // a + (b + c) -> a + b + c
  [Blockly.Arduino.ORDER_ADDITION, Blockly.Arduino.ORDER_ADDITION],
  // a && (b && c) -> a && b && c
  [Blockly.Arduino.ORDER_LOGICAL_AND, Blockly.Arduino.ORDER_LOGICAL_AND],
  // a || (b || c) -> a || b || c
  [Blockly.Arduino.ORDER_LOGICAL_OR, Blockly.Arduino.ORDER_LOGICAL_OR]
];

/**
 * Initialise the database of variable names.
 * @param {!Blockly.Workspace} workspace Workspace to generate code from.
 */
Blockly.Arduino.init = function(workspace) {
  // Create a dictionary of definitions to be printed before the code.
  Blockly.Arduino.definitions_ = Object.create(null);
  // Create a dictionary mapping desired function names in definitions_
  // to actual function names (to avoid collisions with user functions).
  Blockly.Arduino.functionNames_ = Object.create(null);

  if (!Blockly.Arduino.variableDB_) {
    Blockly.Arduino.variableDB_ =
        new Blockly.Names(Blockly.Arduino.RESERVED_WORDS_);
  } else {
    Blockly.Arduino.variableDB_.reset();
  }

  Blockly.Arduino.variableDB_.setVariableMap(workspace.getVariableMap());

  var defvars = [];
  // Add developer variables (not created or named by the user).
  var devVarList = Blockly.Variables.allDeveloperVariables(workspace);
  for (var i = 0; i < devVarList.length; i++) {
    defvars.push(Blockly.Arduino.variableDB_.getName(devVarList[i],
        Blockly.Names.DEVELOPER_VARIABLE_TYPE));
  }

  // Add user variables, but only ones that are being used.
  var variables = Blockly.Variables.allUsedVarModels(workspace);
  for (var i = 0; i < variables.length; i++) {
    defvars.push(Blockly.Arduino.variableDB_.getName(variables[i].getId(),
        Blockly.Variables.NAME_TYPE));
  }

  // Declare all of the variables.
  if (defvars.length) {
    Blockly.Arduino.definitions_['variables'] =
        'int ' + defvars.join(', ') + ';';
  }
};

/**
 * Prepend the generated code with the variable definitions.
 * @param {string} code Generated code.
 * @return {string} Completed code.
 */
Blockly.Arduino.finish = function(code) {

  // TODO: Look up for libraries to initialize here
  var setupCode = "void setup() {\r\n" +
  "  Serial.begin(9600);\r\n" +
  "}\r\n";

  // Convert the definitions dictionary into a list.
  var definitions = [];
  for (var name in Blockly.Arduino.definitions_) {
    definitions.push(Blockly.Arduino.definitions_[name]);
  }
  // Clean up temporary data.
  delete Blockly.Arduino.definitions_;
  delete Blockly.Arduino.functionNames_;
  Blockly.Arduino.variableDB_.reset();

  var loopCode = "void loop() {\r\n" + code +
  "}\r\n";

  return definitions.join('\r\n') + '\r\n' + setupCode + '\r\n' + loopCode;
};

/**
 * Naked values are top-level blocks with outputs that aren't plugged into
 * anything.  A trailing semicolon is needed to make this legal.
 * @param {string} line Line of generated code.
 * @return {string} Legal line of code.
 */
Blockly.Arduino.scrubNakedValue = function(line) {
  return line + ';\n';
};

/**
 * Encode a string as a properly escaped JavaScript string, complete with
 * quotes.
 * @param {string} string Text to encode.
 * @return {string} JavaScript string.
 * @private
 */
Blockly.Arduino.quote_ = function(string) {
  // Can't use goog.string.quote since Google's style guide recommends
  // JS string literals use single quotes.
  string = string.replace(/\\/g, '\\\\')
                 .replace(/\n/g, '\\\n')
                 .replace(/"/g, '\\\"');
  return '"' + string + '"';
};

/**
 * Common tasks for generating JavaScript from blocks.
 * Handles comments for the specified block and any connected value blocks.
 * Calls any statements following this block.
 * @param {!Blockly.Block} block The current block.
 * @param {string} code The JavaScript code created for this block.
 * @return {string} JavaScript code with comments and subsequent blocks added.
 * @private
 */
Blockly.Arduino.scrub_ = function(block, code) {
  var commentCode = '';
  // Only collect comments for blocks that aren't inline.
  if (!block.outputConnection || !block.outputConnection.targetConnection) {
    // Collect comment for this block.
    var comment = block.getCommentText();
    comment = Blockly.utils.wrap(comment, Blockly.Arduino.COMMENT_WRAP - 3);
    if (comment) {
      if (block.getProcedureDef) {
        // Use a comment block for function comments.
        commentCode += '/**\n' +
                       Blockly.Arduino.prefixLines(comment + '\n', ' * ') +
                       ' */\n';
      } else {
        commentCode += Blockly.Arduino.prefixLines(comment + '\n', '// ');
      }
    }
    // Collect comments for all value arguments.
    // Don't collect comments for nested statements.
    for (var i = 0; i < block.inputList.length; i++) {
      if (block.inputList[i].type == Blockly.INPUT_VALUE) {
        var childBlock = block.inputList[i].connection.targetBlock();
        if (childBlock) {
          var comment = Blockly.Arduino.allNestedComments(childBlock);
          if (comment) {
            commentCode += Blockly.Arduino.prefixLines(comment, '// ');
          }
        }
      }
    }
  }
  var nextBlock = block.nextConnection && block.nextConnection.targetBlock();
  var nextCode = Blockly.Arduino.blockToCode(nextBlock);
  return commentCode + code + nextCode;
};

/**
 * Gets a property and adjusts the value while taking into account indexing.
 * @param {!Blockly.Block} block The block.
 * @param {string} atId The property ID of the element to get.
 * @param {number=} opt_delta Value to add.
 * @param {boolean=} opt_negate Whether to negate the value.
 * @param {number=} opt_order The highest order acting on this value.
 * @return {string|number}
 */
Blockly.Arduino.getAdjusted = function(block, atId, opt_delta, opt_negate,
    opt_order) {
  var delta = opt_delta || 0;
  var order = opt_order || Blockly.Arduino.ORDER_NONE;
  if (block.workspace.options.oneBasedIndex) {
    delta--;
  }
  var defaultAtIndex = block.workspace.options.oneBasedIndex ? '1' : '0';
  if (delta > 0) {
    var at = Blockly.Arduino.valueToCode(block, atId,
        Blockly.Arduino.ORDER_ADDITION) || defaultAtIndex;
  } else if (delta < 0) {
    var at = Blockly.Arduino.valueToCode(block, atId,
        Blockly.Arduino.ORDER_SUBTRACTION) || defaultAtIndex;
  } else if (opt_negate) {
    var at = Blockly.Arduino.valueToCode(block, atId,
        Blockly.Arduino.ORDER_UNARY_NEGATION) || defaultAtIndex;
  } else {
    var at = Blockly.Arduino.valueToCode(block, atId, order) ||
        defaultAtIndex;
  }

  if (Blockly.isNumber(at)) {
    // If the index is a naked number, adjust it right now.
    at = parseFloat(at) + delta;
    if (opt_negate) {
      at = -at;
    }
  } else {
    // If the index is dynamic, adjust it in code.
    if (delta > 0) {
      at = at + ' + ' + delta;
      var innerOrder = Blockly.Arduino.ORDER_ADDITION;
    } else if (delta < 0) {
      at = at + ' - ' + -delta;
      var innerOrder = Blockly.Arduino.ORDER_SUBTRACTION;
    }
    if (opt_negate) {
      if (delta) {
        at = '-(' + at + ')';
      } else {
        at = '-' + at;
      }
      var innerOrder = Blockly.Arduino.ORDER_UNARY_NEGATION;
    }
    innerOrder = Math.floor(innerOrder);
    order = Math.floor(order);
    if (innerOrder && order >= innerOrder) {
      at = '(' + at + ')';
    }
  }
  return at;
};

/*
Blockly.Arduino.workspaceToCode = function(workspace) {
  var setupCode = "void setup() {\r\n" +
  "  Serial.begin(9600);\r\n" +
  "}\r\n";

  var loopCode = "void loop() {\r\n" +
  Blockly.Generator.prototype.workspaceToCode.call(this,workspace) +
  "}\r\n";

  return setupCode+"\r\n"+loopCode;
};
*/